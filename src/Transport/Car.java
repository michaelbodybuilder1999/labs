package Transport;

import Engine.*;
import Other.*;
import com.google.gson.Gson;

import javax.xml.bind.annotation.*;

@XmlRootElement(name="car")
@XmlAccessorType(XmlAccessType.FIELD)
public class Car extends Transport {

    public static final int MIN_DIAMETER = 12;
    public static final int MAX_DIAMETER = 32;

    @XmlElements({
            @XmlElement(name="icengine", type = ICEngine.class),
            @XmlElement(name="eengine", type = EEngine.class),
    })

    private Engine engine;
    private Transmission transmission;

    private int engines;
    private Drive drive;
    private int shafts;
    private int diameter;

    private double energy_carrier;
    private int luggage_capacity;

    private boolean trailer;
    private boolean autopilot;

    public Car()
    {
        engine = new ICEngine();
        transmission = new Transmission();
    }

    @Override
    public String toString() {
        String res = "";

        res += "Car\n";

        res += "Producer " + getProducer() + "\n";
        res += "Model " + getModel() + "\n";
        res += "Mileage " + getMileage() + "\n";
        res += "Release " + getRelease() + "\n";
        res += "Age " + getAge().getYear() + " years, " + (getAge().getMonth().getValue() - 1) + " months, " + (getAge().getDayOfMonth() - 2) + " days\n\n";

        res += engine;
        res += "Number of engines " + engines + "\n\n";

        res += transmission + "\n";

        res += getFrame();
        res += "Frame Number " + getFrameNumber() + "\n\n";

        switch (drive)
        {
            case FWD: res += "Drive FWD\n"; break;
            case RWD: res += "Drive RWD\n"; break;
            case FourWD: res += "Drive 4WD\n"; break;
            case AWD: res += "Drive AWD\n"; break;
        }

        res += "Number of shafts " + shafts + "\n";

        res += "Energy carrier " + energy_carrier;
        if(engine.getClass() == ICEngine.class) res += "L\n";
        if(engine.getClass() == EEngine.class) res += "Wh\n";

        res += "Max mileage " + getMaxMileage() + "\n";

        res += "Luggage capacity " + luggage_capacity + "\n";
        res += "Has trailer " + trailer + "\n";
        res += "Has autopilot " + autopilot + "\n";
        res += "Wheels diameter " + diameter + "\n";

        return res;
    }

    public double getMaxMileage()
    {
        return energy_carrier * 100 / engine.getConsumption();
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    public int getEngines() {
        return engines;
    }

    public void setEngines(int engines) {
        this.engines = engines;
    }

    public Drive getDrive() {
        return drive;
    }

    public void setDrive(Drive drive) {
        this.drive = drive;
    }

    public int getShafts() {
        return shafts;
    }

    public void setShafts(int shafts) {
        this.shafts = shafts;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public double getEnergy_carrier() {
        return energy_carrier;
    }

    public void setEnergy_carrier(double energy_carrier) {
        this.energy_carrier = energy_carrier;
    }

    public int getLuggage_capacity() {
        return luggage_capacity;
    }

    public void setLuggage_capacity(int luggage_capacity) {
        this.luggage_capacity = luggage_capacity;
    }

    public boolean hasTrailer() {
        return trailer;
    }

    public void setTrailer(boolean trailer) {
        this.trailer = trailer;
    }

    public boolean isAutopilot() {
        return autopilot;
    }

    public void setAutopilot(boolean autopilot) {
        this.autopilot = autopilot;
    }
}