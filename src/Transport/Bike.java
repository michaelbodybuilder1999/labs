package Transport;

public class Bike extends Transport {

    public static final int MIN_DIAMETER = 18;
    public static final int MAX_DIAMETER = 30;

    private int id;

    private String front_susp;
    private String rear_susp;
    private double diameter;
    private int gears;

    @Override
    public String toString() {
        String res = "";

        res += "Bike\n";

        res += "Producer " + getProducer() + "\n";
        res += "Model " + getModel() + "\n";
        res += "Mileage " + getMileage() + "\n";
        res += "Release " + getRelease() + "\n";
        res += "Age " + getAge().getYear() + " years, " + (getAge().getMonth().getValue() - 1) + " months, " + (getAge().getDayOfMonth() - 2) + " days\n\n";

        res += getFrame() + "\n";

        res += "Front suspension " + front_susp + "\n";
        res += "Rear suspension " + rear_susp + "\n";
        res += "Wheels diameter " + diameter + "\n";
        res += "Gears " + gears + "\n";

        return res;
    }

    public String getFront_susp() {
        return front_susp;
    }

    public void setFront_susp(String front_susp) {
        this.front_susp = front_susp;
    }

    public String getRear_susp() {
        return rear_susp;
    }

    public void setRear_susp(String rear_susp) {
        this.rear_susp = rear_susp;
    }

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    public int getGears() {
        return gears;
    }

    public void setGears(int gears) {
        this.gears = gears;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}