package Transport;

import Other.*;
import com.sun.xml.internal.txw2.annotation.XmlElement;

import javax.xml.bind.annotation.*;
import java.time.LocalDate;

public abstract class Transport {

    private Frame frame;
    private String frameNumber;

    private int seats;

    private int mileage;

    private String producer;
    private String model;
    private MyDate release;

    public Transport()
    {
        frame = new Frame();
        frameNumber = "";
        producer = "";
        model = "";
        release = new MyDate();
    }

    public LocalDate getAge()
    {
        return LocalDate.now().minusYears(release.getYear()).minusDays(release.getDayOfYear() - 1);
    }

    public Frame getFrame() {
        return frame;
    }

    public void setFrame(Frame frame) {
        this.frame = frame;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public MyDate getRelease() {
        return release;
    }

    public void setRelease(MyDate release) {
        this.release = release;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public String getFrameNumber() {
        return frameNumber;
    }

    public void setFrameNumber(String frameNumber) {
        this.frameNumber = frameNumber;
    }
}
