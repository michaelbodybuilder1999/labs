package Transport;

import Engine.*;
import Other.Transmission;

import javax.xml.bind.annotation.*;

@XmlRootElement(name="motorbike")
@XmlAccessorType(XmlAccessType.FIELD)
public class Motorbike extends Transport {

    public static final int MIN_DIAMETER = 18;
    public static final int MAX_DIAMETER = 30;

    @XmlElements({
            @XmlElement(name="icengine", type = ICEngine.class),
            @XmlElement(name="eengine", type = EEngine.class),
    })

    private Engine engine;
    private Transmission transmission;

    private String rear_susp;
    private String front_susp;
    private int rear_diam;
    private int front_diam;

    private int energy_carrier;

    public Motorbike()
    {
        engine = new ICEngine();
        transmission = new Transmission();
    }

    @Override
    public String toString() {
        String res = "";

        res += "Motorbike\n";

        res += "Producer " + getProducer() + "\n";
        res += "Model " + getModel() + "\n";
        res += "Mileage " + getMileage() + "\n";
        res += "Release " + getRelease() + "\n";
        res += "Age " + getAge().getYear() + " years, " + (getAge().getMonth().getValue() - 1) + " months, " + (getAge().getDayOfMonth() - 2) + " days\n\n";

        res += engine + "\n";

        res += transmission + "\n";

        res += getFrame() + "\n";

        res += "Energy carrier " + energy_carrier;
        if(engine.getClass() == ICEngine.class) res += "L\n";
        if(engine.getClass() == EEngine.class) res += "Wh\n";

        res += "Max mileage " + getMaxMileage() + "\n";

        res += "Front suspension " + front_susp + "\n";
        res += "Rear suspension " + rear_susp + "\n";
        res += "Front wheel diameter " + front_diam + "\n";
        res += "Rear wheel diameter " + rear_diam + "\n";

        return res;
    }

    public double getMaxMileage()
    {
        return energy_carrier * 100 / engine.getConsumption();
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    public String getRear_susp() {
        return rear_susp;
    }

    public void setRear_susp(String rear_susp) {
        this.rear_susp = rear_susp;
    }

    public String getFront_susp() {
        return front_susp;
    }

    public void setFront_susp(String front_susp) {
        this.front_susp = front_susp;
    }

    public int getRear_diam() {
        return rear_diam;
    }

    public void setRear_diam(int rear_diam) {
        this.rear_diam = rear_diam;
    }

    public int getFront_diam() {
        return front_diam;
    }

    public void setFront_diam(int front_diam) {
        this.front_diam = front_diam;
    }

    public int getEnergy_carrier() {
        return energy_carrier;
    }

    public void setEnergy_carrier(int energy_carrier) {
        this.energy_carrier = energy_carrier;
    }
}