package XmlJson;

import Garage.Garage;
import Transport.*;
import com.google.gson.Gson;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

public class GarageSerialize implements Serialized
{
    public void serializeXml(Garage garage, String path) throws JAXBException
    {
        JAXBContext jaxbContext = JAXBContext.newInstance(Garage.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(garage, new File(path));
    }

    public Garage deserializeXml(String path) throws JAXBException
    {
        JAXBContext jaxbContext = JAXBContext.newInstance(Garage.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        Garage garage = (Garage) jaxbUnmarshaller.unmarshal(new File(path));
        return garage;
    }

    public void serializeJson(Garage garage, String filepath) throws IOException
    {
        Gson gson = new Gson();

        File cars = new File(filepath + "Cars.json");
        File motos = new File(filepath + "Motorbikes.json");
        File bikes = new File(filepath + "Bikes.json");

        FileWriter fw1 = new FileWriter(cars);
        FileWriter fw2 = new FileWriter(motos);
        FileWriter fw3 = new FileWriter(bikes);

        CarsList carsList = new CarsList();
        MotosList motosList = new MotosList();
        BikesList bikesList = new BikesList();

        for(Transport t : garage.getTransports())
        {
            if(t.getClass() == Car.class)
            {
                carsList.list.add((Car)t);
            }
            if(t.getClass() == Motorbike.class)
            {
                motosList.list.add((Motorbike)t);
            }
            if(t.getClass() == Bike.class)
            {
                bikesList.list.add((Bike) t);
            }
        }

        fw1.write(gson.toJson(carsList));
        fw2.write(gson.toJson(motosList));
        fw3.write(gson.toJson(bikesList));

        fw1.close();
        fw2.close();
        fw3.close();
    }

    public Garage deserializeJson(String filepath) throws IOException
    {
        Gson gson = new Gson();

        File cars = new File(filepath + "Cars.json");
        File motos = new File(filepath + "Motorbikes.json");
        File bikes = new File(filepath + "Bikes.json");

        Garage garage = new Garage();

        CarsList carsList = gson.fromJson(new FileReader(cars), CarsList.class);
        MotosList motosList = gson.fromJson(new FileReader(motos), MotosList.class);
        BikesList bikesList = gson.fromJson(new FileReader(bikes), BikesList.class);

        garage.getTransports().addAll(carsList.list);
        garage.getTransports().addAll(motosList.list);
        garage.getTransports().addAll(bikesList.list);

        return garage;
    }

    private class CarsList {
        public LinkedList<Car> list;

        public CarsList()
        {
            list = new LinkedList<>();
        }
    }

    private class MotosList {
        public LinkedList<Motorbike> list;

        public MotosList()
        {
            list = new LinkedList<>();
        }
    }

    private class BikesList {
        public LinkedList<Bike> list;

        public BikesList()
        {
            list = new LinkedList<>();
        }
    }
}