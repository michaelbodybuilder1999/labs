package XmlJson;

import Garage.Garage;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public interface Serialized {

     void serializeXml(Garage garage, String filename) throws JAXBException;
     Garage deserializeXml(String filename) throws JAXBException;

     void serializeJson(Garage garage, String filepath) throws IOException;
     Garage deserializeJson(String filepath) throws IOException;

}
