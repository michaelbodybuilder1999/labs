package XmlJson;

import Engine.*;
import Other.*;
import Transport.*;
import Garage.*;

import java.time.LocalDate;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ICEngine engine = new ICEngine();

        engine.setProducer("BMW");
        engine.setPower(300);
        engine.setTorque(120);
        engine.setVolume(3600);
        engine.setCylinders(6);
        engine.setTurbine(true);
        engine.setUpper(true);
        engine.setType(ICEngine.DIESEL);
        engine.setStandard(ICEngine.EURO2);
        engine.setWeight(250);
        engine.setConsumption(12);
        engine.setRelease(new MyDate(LocalDate.now().minusYears(12).minusDays(240)));

        ICEngine engine1 = new ICEngine();

        engine1.setPower(50);
        engine1.setTorque(80);
        engine1.setVolume(500);
        engine1.setCylinders(2);
        engine1.setTurbine(false);
        engine1.setUpper(true);
        engine1.setType(ICEngine.GASOLINE);
        engine1.setStandard(ICEngine.EURO2);
        engine1.setWeight(50);
        engine1.setConsumption(4);
        engine1.setRelease(new MyDate(LocalDate.now().minusYears(15).minusDays(180)));

        EEngine engine2 = new EEngine();
        engine2.setProducer("Tesla Motors");
        engine2.setPower(50);
        engine2.setTorque(80);
        engine2.setWeight(50);
        engine2.setConsumption(4);
        engine2.setAmperage(10);
        engine2.setVoltage(40);
        engine2.setRelease(new MyDate(LocalDate.now().minusYears(15).minusDays(180)));

        Frame frame = new Frame();

        frame.setProducer("BMW");
        frame.setMaterial(Material.ALUMINIUM);
        frame.setWeight(500);
        frame.setRelease(new MyDate(LocalDate.now().minusYears(12).minusDays(280)));

        Frame frame1 = new Frame();

        frame1.setProducer("BMW");
        frame1.setMaterial(Material.ALUMINIUM);
        frame1.setWeight(30);
        frame1.setRelease(new MyDate(LocalDate.now().minusYears(15).minusDays(280)));

        Frame frame2 = new Frame();

        frame2.setProducer("Scott");
        frame2.setMaterial(Material.ALUMINIUM);
        frame2.setWeight(3);
        frame2.setRelease(new MyDate(LocalDate.now().minusYears(5).minusDays(50)));

        Transmission transmission = new Transmission();

        transmission.setType(TransType.MECHANIC);
        transmission.setGears(6);
        transmission.setProducer("BMW");
        transmission.setWeight(100);
        transmission.setRelease(new MyDate(LocalDate.now().minusYears(12).minusDays(260)));

        Transmission transmission1 = new Transmission();

        transmission1.setType(TransType.MECHANIC);
        transmission1.setGears(4);
        transmission1.setProducer("BMW");
        transmission1.setWeight(20);
        transmission1.setRelease(new MyDate(LocalDate.now().minusYears(15).minusDays(260)));

        Car bmw = new Car();

        bmw.setProducer("BMW");
        bmw.setModel("3");
        bmw.setFrameNumber("BMW-3");
        bmw.setEngine(engine);
        bmw.setTransmission(transmission);
        bmw.setFrame(frame);
        bmw.setDrive(Drive.RWD);
        bmw.setEngines(1);
        bmw.setShafts(2);
        bmw.setDiameter(16);
        bmw.setAutopilot(false);
        bmw.setTrailer(false);
        bmw.setSeats(5);
        bmw.setMileage(265120);
        bmw.setRelease(new MyDate(LocalDate.now().minusYears(12).minusDays(80)));
        bmw.setEnergy_carrier(50);
        bmw.setLuggage_capacity(300);

        //System.out.println(bmw);

        Motorbike motorbike = new Motorbike();

        motorbike.setProducer("BMW");
        motorbike.setModel("MB");
        motorbike.setFrameNumber("BMW-MB");
        motorbike.setEngine(engine2);
        motorbike.setTransmission(transmission1);
        motorbike.setFrame(frame1);
        motorbike.setSeats(2);
        motorbike.setMileage(15300);
        motorbike.setRelease(new MyDate(LocalDate.now().minusYears(15).minusDays(100)));
        motorbike.setEnergy_carrier(20);
        motorbike.setFront_susp("FOX 150 mm coil");
        motorbike.setRear_susp("FOX 120 mm coil");
        motorbike.setFront_diam(20);
        motorbike.setRear_diam(18);

        //System.out.println(motorbike);

        Bike bike = new Bike();

        bike.setProducer("Scott");
        bike.setModel("Aspect");
        bike.setFrameNumber("SCOTT-ASPECT");
        bike.setFrame(frame2);
        bike.setSeats(1);
        bike.setMileage(2500);
        bike.setRelease(new MyDate(LocalDate.now().minusYears(5).minusDays(150)));
        bike.setGears(30);
        bike.setFront_susp("Fox 150 mm air");
        bike.setRear_susp("Fox 150 mm air");
        bike.setDiameter(27.5);

        //System.out.println(bike);

        ArrayList<Transport> list = new ArrayList<>();
        list.add(bmw);
        list.add(bike);
        list.add(motorbike);

        Garage garage = GarageBuilder.buildGarage(list);

        garage.writeToXml();
        garage.writeToJson();

        //System.out.println(garage);

        try
        {
            GarageSerialize serialize = new GarageSerialize();

            garage = serialize.deserializeXml("NewGarage.xml");
            System.out.println(garage);

           garage = serialize.deserializeJson("");
           System.out.println(garage);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        //List<Transport> list = garage.getMileageMore(10000);

        //System.out.println(Arrays.toString(list.toArray()));
    }
}
