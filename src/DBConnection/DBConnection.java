package DBConnection;

import Other.Frame;
import Other.Material;
import Other.MyDate;
import Transport.Bike;

import java.sql.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.*;

public class DBConnection {

    private static final String URL = "jdbc:mysql://127.0.0.1/garage?serverTimezone=" + TimeZone.getDefault().getID();
    private static final String USER = "root";
    private static final String PASSWORD = "5799";

    private Connection connection;

    public DBConnection() throws SQLException
    {
        Properties properties = new Properties();
        properties.setProperty("user", USER);
        properties.setProperty("password", PASSWORD);
        properties.setProperty("useSSL", "false");
        properties.setProperty("autoReconnect", "true");
        connection = DriverManager.getConnection( URL, properties );
    }

    public ArrayList<Frame> getFrames() throws SQLException
    {
        ArrayList<Frame> frames = new ArrayList<>();

        Statement statement = connection.createStatement();
        ResultSet set = statement.executeQuery("SELECT * FROM `frame`");

        while(set.next())
        {
            int idframe = set.getInt("idframe");
            int weight = set.getInt("weight");
            String producer = set.getString("producer");
            String material = set.getString("material");
            Date date = set.getDate("release");

            MyDate release = new MyDate(LocalDate.parse(date.toString()));

            Material material1 = Material.STEEL;

            switch (material.toLowerCase())
            {
                case "aluminium":
                {
                    material1 = Material.ALUMINIUM;
                    break;
                }
                case "carbon":
                {
                    material1 = Material.CARBON;
                    break;
                }
                case "composite":
                {
                    material1 = Material.COMPOSITE;
                    break;
                }
                case "fiberglass":
                {
                    material1 = Material.FIBERGLASS;
                    break;
                }
            }

            Frame frame = new Frame();
            frame.setId(idframe);
            frame.setWeight(weight);
            frame.setMaterial(material1);
            frame.setProducer(producer);
            frame.setRelease(release);

            frames.add(frame);
        }

        return frames;
    }

    public void insertFrame(Frame frame) throws SQLException
    {
        Statement statement = connection.createStatement();

        String material = "Steel";

        switch (frame.getMaterial())
        {
            case ALUMINIUM:
            {
                material = "Aluminium";
                break;
            }
            case CARBON:
            {
                material = "Carbon";
                break;
            }
            case FIBERGLASS:
            {
                material = "Fiberglass";
                break;
            }
            case COMPOSITE:
            {
                material = "Composite";
                break;
            }
        }

        String sql = "INSERT INTO `garage`.`frame` (`idframe`, `weight`, `material`, `producer`, `release`) VALUES ('"+frame.getId()+"', '"+frame.getWeight()+"', '"+material+"', '"+frame.getProducer()+"', '"+frame.getRelease()+"')";
        statement.executeUpdate(sql);
    }

    public void clearFrames() throws SQLException
    {
        Statement statement = connection.createStatement();
        statement.executeUpdate("truncate table frame");
    }

    public ArrayList<Bike> getBikes() throws SQLException
    {
        ArrayList<Bike> bikes = new ArrayList<>();

        Statement statement = connection.createStatement();
        ResultSet set = statement.executeQuery("SELECT * FROM `bike`");

        while(set.next()) {
            int idbike = set.getInt("idbike");

            int idframe = set.getInt("idframe");
            Frame frame = null;
            for (Frame f : getFrames())
            {
                if(f.getId() == idframe)
                {
                    frame = f;
                    break;
                }
            }

            String frameNumber = set.getString("framenumber");
            int seats = set.getInt("seats");
            int mileage = set.getInt("mileage");
            String producer = set.getString("producer");
            String model = set.getString("model");
            Date date = set.getDate("release");

            String frontsusp = set.getString("frontsusp");
            String rearsusp = set.getString("rearsusp");
            int diameter = set.getInt("diameter");
            int gears = set.getInt("gears");

            MyDate release = new MyDate(LocalDate.parse(date.toString()));

            Bike bike = new Bike();

            bike.setFrame(frame);
            bike.setId(idbike);
            bike.setProducer(producer);
            bike.setRelease(release);
            bike.setMileage(mileage);
            bike.setSeats(seats);
            bike.setModel(model);
            bike.setFrameNumber(frameNumber);

            bike.setFront_susp(frontsusp);
            bike.setRear_susp(rearsusp);
            bike.setDiameter(diameter);
            bike.setGears(gears);

            bikes.add(bike);

        }
        return bikes;
    }

    public void insertBike(Bike bike) throws SQLException
    {
        Statement statement = connection.createStatement();

        String sql = "INSERT INTO `garage`.`bike` (`idbike`, `idframe`, `framenumber`, `seats`, `mileage`, `producer`, `model`, `release`, `frontsusp`, `rearsusp`, `diameter`, `gears`) VALUES ('"+bike.getId()+"', '"+bike.getFrame().getId()+"', '"+bike.getFrameNumber()+"', '"+bike.getSeats()+"', '"+bike.getMileage()+"', '"+bike.getProducer()+"', '"+bike.getModel()+"', '"+bike.getRelease()+"', '"+bike.getFront_susp()+"', '"+bike.getRear_susp()+"', '"+bike.getDiameter()+"', '"+bike.getGears()+"');";
        statement.executeUpdate(sql);
    }

    public void clearBikes() throws SQLException
    {
        Statement statement = connection.createStatement();
        statement.executeUpdate("truncate table bike");
    }

    public static void main(String[] args) {
        try
        {
            DBConnection db = new DBConnection();

            ArrayList<Bike> bikes = db.getBikes();
            System.out.println(bikes);
            ArrayList<Frame> frames = db.getFrames();
            System.out.println(frames);

            db.clearBikes();
            db.clearFrames();

            for(Frame frame : frames)
            {
                db.insertFrame(frame);
            }
            for(Bike bike : bikes)
            {
                db.insertBike(bike);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}
