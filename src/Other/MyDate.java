package Other;

import java.time.LocalDate;

public class MyDate {

    private int year, month, day;

    public MyDate()
    {
        LocalDate date = LocalDate.now();

        year = date.getYear();
        month = date.getMonthValue();
        day = date.getDayOfMonth();
    }

    public MyDate(LocalDate date)
    {
        year = date.getYear();
        month = date.getMonthValue();
        day = date.getDayOfMonth();
    }

    @Override
    public String toString() {
        return LocalDate.of(year, month, day).toString();
    }

    @Override
    public boolean equals(Object obj) {

        if(obj == this)
            return true;

        if(obj == null || obj.getClass() != getClass())
            return false;

        MyDate date = (MyDate)obj;

        return date.year == year && date.month == month && date.day == day;
    }

    @Override
    public int hashCode() {

        long res = 0, g;

        int[] mas = new int[3];
        mas[0] = year;
        mas[1] = month;
        mas[2] = day;

        for(int i = 0; i < mas.length; ++i)
        {
            res = (res << 4) + mas[i];
            g = res & 0xf0000000L;

            if (g != 0)
            {
                res ^= g >> 24;
            }

            res &= ~g;
        }

        return (int)res;
    }

    public int getDayOfYear()
    {
        LocalDate date = LocalDate.of(year, month, day);

        return date.getDayOfYear();
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }
}
