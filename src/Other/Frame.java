package Other;

public class Frame {

    private int id;

    private Material material;
    private int weight;

    private String producer;
    private MyDate release;

    public Frame()
    {
        producer = "";
        release = new MyDate();
    }

    /*public LocalDate getAge()
    {
        return LocalDate.now().minusYears(released.getYear()).minusDays(released.getDayOfYear() - 1);
    }*/

    @Override
    public String toString()
    {
        String res = "Frame\n";

        res += "Producer " + producer + "\n";

        switch (material)
        {
            case STEEL: res += "Material Steel\n"; break;
            case ALUMINIUM: res += "Material Aluminium\n"; break;
            case CARBON: res += "Material Carbon\n"; break;
            case FIBERGLASS: res += "Material Fiberglass\n"; break;
            case COMPOSITE: res += "Material Composite\n"; break;
        }

        res += "Weight " + weight + " kg\n";
        res += "Release date " + release + "\n";

        return res;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public MyDate getRelease() {
        return release;
    }

    public void setRelease(MyDate released) {
        this.release = released;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
