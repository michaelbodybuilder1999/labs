package Other;

import java.time.LocalDate;

public class Transmission {

    private TransType type;
    private int gears;
    private int weight;

    private String producer;
    private MyDate release;

    public Transmission()
    {
        producer = "";
        release = new MyDate();
    }

    public LocalDate getAge()
    {
        return LocalDate.now().minusYears(release.getYear()).minusDays(release.getDayOfYear() - 1);
    }

    @Override
    public String toString()
    {
        String res = "Transmission\n";

        res += "Producer " + producer + "\n";

        switch (type)
        {
            case AUTOMATIC: res += "Type Automatic\n"; break;
            case MECHANIC: res += "Type Mechanic\n"; break;
            case SEMI_AUTOMATIC: res += "Type Semi-Automatic\n"; break;

        }

        res += "Gears " + gears + "\n";
        res += "Weight " + weight + " kg\n";
        res += "Release date " + release + "\n";

        return res;
    }

    public TransType getType() {
        return type;
    }

    public void setType(TransType type) {
        this.type = type;
    }

    public int getGears() {
        return gears;
    }

    public void setGears(int gears) {
        this.gears = gears;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public MyDate getRelease() {
        return release;
    }

    public void setRelease(MyDate release) {
        this.release = release;
    }
}