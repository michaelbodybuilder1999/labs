package Garage;

import Transport.*;

import java.util.List;

public class GarageBuilder {

    public static Garage buildGarage(List<Transport> transports)
    {
        Garage garage = new Garage();

        checkTransports(transports);

        garage.getTransports().addAll(transports);

        return garage;
    }

    private static void checkTransports(List<Transport> transports)
    {
        if(transports == null)
            throw new NullPointerException("List of transports isn't initialized!");

        for(Transport t : transports)
        {
            if(t.getFrame() == null)
                throw new NullPointerException("Frame is not set!");

            if(t.getClass() == Car.class)
            {
                Car car = (Car)t;
                if(car.getDiameter() < Car.MIN_DIAMETER || car.getDiameter() > Car.MAX_DIAMETER)
                    throw new IllegalArgumentException("Wheel diameter is incorrect!");
                if(car.getEngine() == null)
                    throw  new NullPointerException("Engine is not set!");
                if(car.getTransmission() == null)
                    throw  new NullPointerException("Transmission is not set!");

                continue;
            }

            if(t.getClass() == Motorbike.class)
            {
                Motorbike motorbike = (Motorbike) t;
                if(motorbike.getFront_diam() < Motorbike.MIN_DIAMETER || motorbike.getFront_diam() > Motorbike.MAX_DIAMETER)
                    throw new IllegalArgumentException("Front wheel diameter is incorrect!");
                if(motorbike.getRear_diam() < Motorbike.MIN_DIAMETER || motorbike.getRear_diam() > Motorbike.MAX_DIAMETER)
                    throw new IllegalArgumentException("Rear wheel diameter is incorrect!");
                if(Math.abs(motorbike.getFront_diam() - motorbike.getRear_diam()) > 3)
                    throw new IllegalArgumentException("Difference between front and rear wheel diameters is too large");
                if(motorbike.getEngine() == null)
                    throw  new NullPointerException("Engine is not set!");
                if(motorbike.getTransmission() == null)
                    throw  new NullPointerException("Transmission is not set!");

                continue;
            }

            if(t.getClass() == Bike.class)
            {
                Bike bike = (Bike) t;
                if(bike.getDiameter() < Bike.MIN_DIAMETER || bike.getDiameter() > Bike.MAX_DIAMETER)
                    throw new IllegalArgumentException("Front wheel diameter is incorrect!");
                if(bike.getDiameter() < Bike.MIN_DIAMETER || bike.getDiameter() > Bike.MAX_DIAMETER)
                    throw new IllegalArgumentException("Rear wheel diameter is incorrect!");
            }
        }
    }

}
