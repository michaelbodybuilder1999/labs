package Garage;

import Transport.*;
import XmlJson.*;

import javax.xml.bind.annotation.*;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@XmlRootElement(name = "arms")
@XmlAccessorType(XmlAccessType.FIELD)
public class Garage {

    @XmlElementWrapper(name = "transports")
    @XmlElements({
            @XmlElement(name = "car", type = Car.class),
            @XmlElement(name = "bike", type = Bike.class),
            @XmlElement(name = "motorbike", type = Motorbike.class),
    })

    private LinkedList<Transport> transports;

    public Garage()
    {
        transports = new LinkedList<>();
    }

    public boolean add(Transport transport)
    {
        return transports.add(transport);
    }

    public boolean remove(Transport transport)
    {
        return transports.remove(transport);
    }

    public void writeToXml()
    {
        try
        {
            new GarageSerialize().serializeXml(this, "NewGarage.xml");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void writeToJson()
    {
        try
        {
            new GarageSerialize().serializeJson(this, "");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * sorts garage by mileage
     */
    public void mileageSort()
    {
        transports.sort(Comparator.comparing(Transport::getMileage));
    }

    /**
     * @param mileage is the mileage of transports
     * @return list of transports with mileage bigger than param
     */
    public List<Transport> getMileageMore(int mileage)
    {
        return transports.stream().filter(Transport -> Transport.getMileage() >= mileage).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        String res = "Garage\n\n";

        for(Transport t : transports)
        {
            res += t + "\n";
        }

        return res;
    }

    public LinkedList<Transport> getTransports() {
        return transports;
    }

    public void setTransports(LinkedList<Transport> transports) {
        this.transports = transports;
    }
}
