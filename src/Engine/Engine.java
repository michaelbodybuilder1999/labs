package Engine;

import Other.MyDate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import java.time.LocalDate;

public class Engine {

    private int power;
    private int torque;
    private int weight;
    private double consumption;

    private String producer;
    private MyDate release;

    public Engine()
    {
        producer = "";
        release = new MyDate();
    }

    @Override
    public String toString() {
        String res = "Engine\n";

        res += "Producer " + getProducer() + "\n";
        res += "Power " + getPower() + " hp\n";
        res += "Torque " + getTorque() + " Nm\n";
        res += "Consumption " + getConsumption() + "L/100km\n";
        res += "Weight " + getWeight() + " kg\n";
        res += "Release date " + getRelease() + "\n";

        return res;
    }

    public LocalDate getAge()
    {
        return LocalDate.now().minusYears(release.getYear()).minusDays(release.getDayOfYear() - 1);
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getTorque() {
        return torque;
    }

    public void setTorque(int torque) {
        this.torque = torque;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getConsumption() {
        return consumption;
    }

    public void setConsumption(double consumption) {
        this.consumption = consumption;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public MyDate getRelease() {
        return release;
    }

    public void setRelease(MyDate release) {
        this.release = release;
    }
}


