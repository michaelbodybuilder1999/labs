package Engine;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

public class EEngine extends Engine {

    private int voltage;
    private int amperage;

    public EEngine()
    {

    }

    @Override
    public String toString()
    {
        String res = "Engine\n";

        res += "Producer " + getProducer() + "\n";
        res += "Power " + getPower() + " hp\n";
        res += "Torque " + getTorque() + " Nm\n";
        res += "Voltage " + voltage + " V\n";
        res += "Amperage " + amperage + " A\n";
        res += "Consumption " + getConsumption() + " kW\n";
        res += "Weight " + getWeight() + " kg\n";
        res += "Release date " + getRelease() + "\n";

        return res;
    }

    public int getVoltage() {
        return voltage;
    }

    public void setVoltage(int voltage) {
        this.voltage = voltage;
    }

    public int getAmperage() {
        return amperage;
    }

    public void setAmperage(int amperage) {
        this.amperage = amperage;
    }
}