package Engine;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

public class ICEngine extends Engine {

    public static final int GASOLINE = 1;
    public static final int DIESEL = 2;
    public static final int GAS = 3;

    public static final int EURO2 = 2;
    public static final int EURO3 = 3;
    public static final int EURO4 = 4;
    public static final int EURO5 = 5;
    public static final int EURO6 = 6;

    private int volume;
    private int cylinders;
    private int type;
    private boolean turbine;
    private boolean upper;
    private int standard;

    @Override
    public String toString()
    {
        String res = "Engine\n";

        res += "Producer " + getProducer() + "\n";
        res += "Power " + getPower() + " hp\n";
        res += "Torque " + getTorque() + " Nm\n";
        res += "Volume " + volume + "L\n";
        res += "Cylinders quantity " + cylinders + "\n";
        res += "Consumption " + getConsumption() + "L/100km\n";
        switch (type)
        {
            case GASOLINE: res += "Type Gasoline\n"; break;
            case DIESEL: res += "Type Diesel\n"; break;
            case GAS: res += "Type Gas\n"; break;
        }
        res += "Turbine " + turbine + "\n";
        res += "Upper engine " + upper + "\n";
        switch (standard)
        {
            case EURO2: res += "Standard EURO 2\n"; break;
            case EURO3: res += "Standard EURO 3\n"; break;
            case EURO4: res += "Standard EURO 4\n"; break;
            case EURO5: res += "Standard EURO 5\n"; break;
            case EURO6: res += "Standard EURO 6\n"; break;
        }
        res += "Weight " + getWeight() + " kg\n";
        res += "Release date " + getRelease() + "\n";

        return res;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getCylinders() {
        return cylinders;
    }

    public void setCylinders(int cylinders) {
        this.cylinders = cylinders;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isTurbine() {
        return turbine;
    }

    public void setTurbine(boolean turbine) {
        this.turbine = turbine;
    }

    public boolean isUpper() {
        return upper;
    }

    public void setUpper(boolean upper) {
        this.upper = upper;
    }

    public int getStandard() {
        return standard;
    }

    public void setStandard(int standard) {
        this.standard = standard;
    }
}